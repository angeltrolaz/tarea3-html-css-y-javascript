<?php
// Obtener los datos del formulario de HTML y pasar los datos a sus respectivas variables de PHP
$nombre = $_POST['nombre'];
$apellido = $_POST['apellido'];
$email = $_POST['email'];
$telefono = $_POST['telefono'];

// Conectar a la base de datos PostgreSQL
$dbconn = pg_connect("host=localhost port=5432 dbname=MUNDO user=openpg password=openpgpwd");
if (!$dbconn) {
  die("Conexión fallida: " . pg_last_error());
}

// Preparar la sentencia SQL para insertar los datos en la tabla 'cliente'
$sql = "INSERT INTO cliente (nombre, apellido, correo, telefono) VALUES ($1, $2, $3, $4)";
$params = array($nombre, $apellido, $email, $telefono);
$result = pg_query_params($dbconn, $sql, $params);

// Verificar si la consulta fue exitosa y mostrar un mensaje en la pantalla
if ($result) {
  echo "¡Los datos se han guardado correctamente!";
} else {
  echo "Error al guardar los datos: " . pg_last_error($dbconn);
}

// Liberar los recursos y cerrar la conexión a la base de datos
pg_free_result($result);
pg_close($dbconn);
?>