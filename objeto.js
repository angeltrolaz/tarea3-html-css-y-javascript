// Selecciona el elemento del DOM que representa el formulario con id 'formulario1'
var formulario = document.getElementById('formulario1');

// Agrega un evento al formulario que se activa al enviar el formulario
formulario.addEventListener('submit', function(e) {

  // Prevenga el comportamiento predeterminado del formulario al hacer clic en el botón de enviar (recargar la página)
  e.preventDefault();

  // Muestra un mensaje en la consola del navegador indicando que se ha hecho clic en el botón de guardar
  console.log('Le has dado a guardar');

  // Crea un objeto FormData a partir del formulario seleccionado para enviar los datos
  var datos = new FormData(formulario);

  // Muestra los valores del formulario en la consola del navegador
  console.log(datos);
  console.log(datos.get('nombre'));
  console.log(datos.get('apellido'));
  console.log(datos.get('email'));
  console.log(datos.get('telefono'));

  // Envía los datos del formulario a través de una solicitud fetch utilizando el método POST y el archivo 'guardar-datos.php' como destino
  fetch('guardar-datos.php', {
    method: 'POST',
    body: datos
  })
  .then(function(response) {
    // Extrae la respuesta de la solicitud fetch como texto
    return response.text();
  })
  .then(function(data) {
    // Muestra la respuesta de la solicitud fetch en la consola del navegador
    console.log(data);
  })
  .catch(function(error) {
    // Muestra cualquier error en la consola del navegador
    console.log(error);
  });
});
